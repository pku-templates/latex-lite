\section{Yield measurements}
\label{sec:yield}
Selection A described above is applied to the full data sample.
Figure~\ref{fig:unblind_Xiccp} shows the $M([\proton\Km\pip]_{\Lc})$ and
$m(\Lc\Km\pip)$ distributions in the \Lc mass range from
2270\mevcc to 2306\mevcc. The quantity $m(\Lc\Km\pip)$ is defined as
\begin{equation}
  \label{eq:mXicc}
  m(\Lc\Km\pip) \equiv M([\proton\Km\pip]_{\Lc}\Km\pip) 
                       - M([\proton\Km\pip]_{\Lc}) 
                       + M_{\mathrm{PDG}}(\Lc),
\end{equation}
where $M([\proton\Km\pip]_{\Lc}\Km\pip)$ is 
the reconstructed mass of the \Xiccp candidate,
$M([\proton\Km\pip]_{\Lc})$ is the reconstructed mass of the \Lc candidate,
and $M_{\mathrm{PDG}}(\Lc)$ is the known value of the \Lc mass~\cite{PDG2018}.
As a comparison, the $m(\Lc\Km\pip)$ distribution of the WS control sample
is also shown in the right plot of Fig.~\ref{fig:unblind_Xiccp}.
The dotted red line indicates the mass of the \Xiccp baryon reported by SELEX~\cite{
  Mattson:2002vu,
  Ocherashvili:2004hi},
and the dashed blue line refers to the mass of the \Xiccpp baryon~\cite{
  LHCb-PAPER-2017-018,
  LHCb-PAPER-2018-026}.
The small enhancement below 3500\mevcc, compared to the WS sample, is due to  partially reconstructed $\Xiccpp$ decays. 
There is no excess near a mass of 3520\mevcc.
A small enhancement is seen near a mass of 3620\mevcc.
To determine the statistical significance of this enhancement,
an extended unbinned maximum-likelihood fit is performed to
the $m(\Lc\Km\pip)$ distribution.
The signal component is described with the sum of
a Gaussian function and
a modified Gaussian function with power-law tails on both sides~\cite{Skwarnicki:1986xj}.
The parameters of the signal model are fixed from simulation
except for the common peak position of the two functions that is allowed to vary freely in the fit.
The background component is described by
a second-order Chebyshev polynomial with all parameters free.
A local $p$-value is evaluated with the likelihood ratio test for rejection of the background-only hypothesis assuming a positive signal~\cite{Wilks:1938dza,Narsky:2000}
and is shown in Fig.~\ref{fig:pvalue}.
%defined as $1-F_1(\sqrt{-2 \ln (\mathcal{L}_0/\mathcal{L})})$,
%where $F_1$ is the cumulative distribution function
%of the \chisq distribution with 1 degree of freedom.
The largest local significance, corresponding to $3.1$
standard deviations ($2.7$ standard deviations after considering systematic uncertainties), occurs around 3620\mevcc.
Taking into account the look-elsewhere effect
in the mass range of 3500\mevcc to 3700\mevcc
following Ref.~\cite{Gross:2010qma},
the global $p$-value is $4.2\times10^{-2}$,
corresponding to a significance of $1.7$ standard deviations.
Since no excess above 3 standard deviations is observed,
upper limits on the production ratios are set using the 
 data recorded at $\sqrt{s}=8\tev$ in 2012
and at $\sqrt{s}=13\tev$ in 2016--2018
after applying \mbox{Selection B}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.49\linewidth]{Fig1a.pdf}
  \includegraphics[width=0.49\linewidth]{Fig1b.pdf}
  \caption{Mass distributions of the (left) intermediate \Lc and (right) \Xiccp candidates
  for the full data sample.
  Selection A is applied, including the \Lc mass requirement,
  indicated by the cross-hatched region in the left plot,
  of $2270\mevcc<M([\proton\Km\pip]_{\Lc})<2306\mevcc$.
  The right-sign (RS) $m(\Lc\Km\pip)$ distribution is shown in the right plot,
  along with the wrong-sign (WS) $m(\Lc\Km\pim)$ distribution
  normalised to have the same area.
  The dotted red line at 3518.7\mevcc indicates the mass of the \Xiccp baryon reported by SELEX~\cite{Ocherashvili:2004hi}
  and the dashed blue line at 3621.2\mevcc indicates the mass of the isospin partner, the \Xiccpp baryon~\cite{LHCb-PAPER-2018-026}.}
  \label{fig:unblind_Xiccp}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{Fig2}
  \caption{Local $p$-value (statistical only) at different $\Xiccp$ mass values
  evaluated with the likelihood-ratio test, for the data sets recorded at $\sqrt{s}=7\tev$, $\sqrt{s}=8\tev$ and $\sqrt{s}=13\tev$.
  Selection A is applied, including the \Lc mass requirement
  of $2270\mevcc<M([\proton\Km\pip]_{\Lc})<2306\mevcc$.}
  %\textcolor{red}{This plot needs to be updated with the 2012,2016--2018 samples.}}
  \label{fig:pvalue}
\end{figure}

To measure the production ratios,
it is necessary to determine the yields of the normalisation modes.
The yield determination procedure of the prompt \Lc decays
is complicated by the substantial secondary \Lc contribution
from $\bquark$-hadron decays,
and is done in two steps.
First, the total number of \Lc candidates is 
determined with an extended unbinned maximum-likelihood fit to
the $M([\proton\Km\pip]_{\Lc})$ distribution.
Then, a fit to
the \logchisqip distribution is performed to
discriminate between prompt and secondary \Lc candidates.
Information from the \Lc mass fit is used to constrain
the total number of \Lc candidates.
The shapes of the prompt and secondary \logchisqip distributions
are described by
a Bukin function~\cite{bukin2007fitting}.
The shape parameters of the prompt and secondary components
are determined from simulation,
except for the mean and the width parameters of the Bukin function, which are allowed to vary in the fit. 
The background component is described by
a nonparametric function generated
using the data from the \Lc mass sideband regions.
As an illustration,
the $M([\proton\Km\pip]_{\Lc})$
and \logchisqip distributions
of the $\Lc$ normalisation mode
candidates in the 2018 data set are shown in Fig.~\ref{fig:yield_Lc}.
The prompt \Lc yields are summarised in Table~\ref{tab:yield_summary}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{Fig3a.pdf}
  \includegraphics[width=0.45\linewidth]{Fig3b.pdf}\\
  \caption{Distributions of (left) $M([\proton\Km\pip]_{\Lc})$ and (right) \logchisqip of the selected $\Lc$ candidates
  with associated fit results for the 2018 data set.}
  \label{fig:yield_Lc}
\end{figure}



To determine the \Xiccpp yield, 
an extended unbinned maximum-likelihood fit is
performed to the $m(\Lc\Km\pip\pip)$ distribution,
which is defined in a similar way to Eq.~\ref{eq:mXicc}.
The same signal and background parameterisations are used as for the signal mode.
For the data sample recorded at $\sqrt{s}=13\tev$,
a simultaneous fit is performed to the \mbox{$m(\Lc\Km\pip\pip)$} distributions
of the candidates in the 
2016, 2017 and 2018 data sets with the shared mean and resolution parameter.
As an illustration,
the $m(\Lc\Km\pip\pip)$ distribution for the 2018 data set
is shown in Fig.~\ref{fig:yield_xicc} along with the associated fit result.
The \Xiccpp yields are summarised in Table~\ref{tab:yield_summary}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{Fig4.pdf}
  \caption{Mass distribution of $\Xiccpp$ candidates in the 
  2018 data set. The result of a fit to the distribution is shown. }
  \label{fig:yield_xicc}
\end{figure}
\begin{table}[bt]
  \centering
  \caption{Signal yields for prompt $\Lc\to\proton\Km\pip$ and $\Xiccpp\to\Lc\Km\pip\pip$ normalisation modes,
   split by data-taking period. The integrated luminosity $\mathcal{L}$ is also shown for each data-taking period.}
  \label{tab:yield_summary}
  \begin{tabular}{ccrr}
    \toprule
    Period   & $\mathcal{L}$ [\invfb]& $N(\Lc)$ [$\times 10^{3}$] & $N(\Xiccpp)$ \\
    \midrule
    %2012&   2.08&  1175280$\pm$2532&  38$\pm$10 \\
    %2016&   1.67&  7339421$\pm$12797  & 121$\pm$19 \\
    %2017&   1.71&  9883000$\pm$9000&  153$\pm$22 \\
    %2018&   2.19&  11184000$\pm$13000&  188$\pm$24 \\
    2012&   2.1&  $1175.3\pm2.5$   & $38 \pm10$ \\
    2016&   1.7&  $7339\pm\;12$   & $121\pm19$ \\
    2017&   1.7&  $9883\pm \phantom{0.}9$   & $153\pm22$ \\
    2018&   2.2&  $11184\pm\; 13$  & $188\pm24$ \\
    \bottomrule
  \end{tabular}
\end{table}
