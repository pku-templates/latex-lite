\section{Introduction}%
\label{sec:introduction}

The constituent quark model~\cite{
  GellMann:1964nj,
  Zweig:352337,
  *Zweig:570209}
predicts the existence of 
weakly decaying doubly charmed baryons with
spin-parity $J^{P}=1/2^{+}$. These include
one isospin doublet \Xicc
($\Xiccp=\cquark\cquark\dquark$ and $\Xiccpp=\cquark\cquark\uquark$), 
and one isospin singlet \Omegaccbare ($\Omegacc=\cquark\cquark\squark$).
The masses of the two \Xicc states are predicted to be in the range  
3500 to 3700\mevcc\cite{
  Kerbikov:1987vx,
  Fleck:1989mb,
  Chernyshev:1995gj,
  Gershtein:1998sx,*Gershtein:1998un,*Gershtein:2000nx,
  Itoh:2000um,
  Anikeev:2001rk,
  Kiselev:2001fw,
  Ebert:2002ig,
  Mathur:2002ce,
  He:2004px,
  Roberts:2007ni,
  Valcarce:2008dr,
  Zhang:2008rt,
  wang2010analysis,
  Aliev:2012ru,
  Namekawa:2013vu,
  Sun:2014aya,
  Karliner:2014gca,
  Brown:2014ena,
  Padmanath:2015jea,
  Bali:2015lka,
  Wei:2015gsa,
  Sun:2016wzh,
  Alexandrou:2017xwd,
  Liu:2017xzo,*Liu:2017frj},
with an isospin splitting of 
a few\:\mevcc\cite{Hwang:2008dj,Brodsky:2011zs,Karliner:2017gml}.
Predictions of the $\Xiccp$ lifetime span the range of 50 to 250\fs, 
while the \Xiccpp lifetime is predicted to be three to four times larger
due to the \mbox{$\W$-exchange} contribution in the \Xiccp decay
and the destructive Pauli interference in the \Xiccpp decay~\cite{
  Fleck:1989mb,
  guberina1999inclusive,
  Kiselev:1998sy,
  Likhoded:1999yv,
  Onishchenko:2000yp,
  Anikeev:2001rk,
  Kiselev:2001fw,
  hsi2008lifetime,
  Karliner:2014gca,
  Berezhnoy2016}.

Doubly charmed baryons have been searched for by
several experiments in the past decades. 
The SELEX collaboration reported the observation of the \Xiccp baryon decaying into \Lcp\Km\pip and $\proton \Dp \Km$ final states~\cite{
  Mattson:2002vu,
  Ocherashvili:2004hi},
using a 600\gevc charged hyperon beam impinging on a fixed target.
The mass of the \Xiccp baryon, averaged over the two decay modes, was found to be \mbox{$3518.7 \pm 1.7 \mevcc$}.
The lifetime was measured to be less than 33\fs at 90\% confidence level.
It was estimated that about 20\% of \Lc baryons in the SELEX experiment were produced from \Xiccp decays~\cite{Mattson:2002vu}.
Searches in different production environments
by the FOCUS~\cite{Ratti:2003ez},
\babar~\cite{Aubert:2006qw},
\lhcb~\cite{LHCb-PAPER-2013-049}
and \belle~\cite{Kato:2013ynr} experiments
did not confirm the SELEX results. 
Recently, the \Xiccpp baryon was observed by the LHCb experiment
in the $\Lcp \Km \pip \pip$ final state~\cite{LHCb-PAPER-2017-018},
and confirmed in the $\Xicp \pip$ final
state~\cite{LHCb-PAPER-2018-026}.
The weighted average of the \Xiccpp mass of the two decay modes was determined to be
$3621.24 \pm 0.65\stat \pm 0.31\syst \mevcc$~\cite{LHCb-PAPER-2018-026},
which is about $100\mevcc$ higher than the mass of the \Xiccp baryon reported by SELEX.
The lifetime of the \Xiccpp baryon was measured to be
$\ensuremath{0.256\,^{+0.024}_{-0.022}\stat\pm
0.014\syst\ps}$~\cite{LHCb-PAPER-2018-019},
which establishes its weakly decaying nature.
The $\Xiccpp\to\Dp\proton\Km\pip$ decay has been searched for by the LHCb
collaboration with a data sample corresponding to an integrated luminosity of 1.7\invfb, 
but no signal was found~\cite{LHCb-PAPER-2019-011}.

This paper presents the result of 
a search for the $\Xiccp$ baryon in the mass range from 3400 to 3800\mevcc,
where the $\Xiccp$ baryon is reconstructed through the $\Xiccp\to\Lc\Km\pip$, $\Lc \to \proton\Km\pip$ decay chain. 
The inclusion of charge-conjugate decay processes is implied throughout this paper. 
The data set comprises $pp$ collision data 
recorded with the \lhcb detector at centre-of-mass energies
$\sqrt{s}=7\tev$ in 2011, $\sqrt{s}=8\tev$ in 2012
and $\sqrt{s}=13\tev$ in 2015--2018,
corresponding to an integrated luminosity of 1.1$\invfb$, 2.1$\invfb$
and 5.9\invfb, respectively.
This data sample is about ten times larger than that of the
previous \Xiccp search by the \lhcb collaboration using only 2011 data~\cite{LHCb-PAPER-2013-049}.

The search was performed 
with the whole analysis procedure defined before %unblinding.
inspecting the data in the 3400 to 3800\mevcc mass range.
The analysis strategy is defined as follows: first a search for a $\Xiccp$ signal is performed and the significance of the signal as a function of the $\Xiccp$ mass is evaluated; then if the global significance, after considering the look-elsewhere effect,
is above 3 standard deviations, %(\mbox{$p$-value} of $1.3\times10^{-3}$)
the $\Xiccp$ mass is measured;
otherwise, upper limits are set on the production rates
for different centre-of-mass energies.
Two sets of selections, with different multivariate classifiers and trigger requirements,  
denoted as \mbox{Selection A} and \mbox{Selection B}, are used in these two cases. 
\mbox{Selection A} is used in the signal search and is designed to maximise its sensitivity.
\mbox{Selection B} is optimised for setting upper limits on the ratio of the $\Xiccp$ production rate to that of $\Xiccpp$ and $\Lcp$ baryons. It uses the same selection for $\Lcp$ baryons from $\Xicc$ decays and prompt $\Lcp$ baryons in order to have better control over sources of systematic uncertainty on the ratio. 
For the limit setting, only 
the data recorded at $\sqrt{s}=8\tev$ in 2012
and at $\sqrt{s}=13\tev$ in 2016--2018 is used.
The exclusion of the 2015 data is due to the marginal yield of the \Xiccpp normalisation mode in that data set.
The production ratio, $\mathcal{R}$, is defined as
\begin{equation}
  \label{eq:RLc}
  \mathcal{R}(\Lc) \equiv \frac{\sigma(\Xiccp)\times\BF(\Xiccp \to \Lc\Km\pip)}{\sigma(\Lc)}
\end{equation}
relative to the prompt $\Lcp$ baryons decaying to $\proton\Km\pip$, and 
\begin{equation}
  \label{eq:RXicc}
  \mathcal{R}(\Xiccpp) \equiv \frac{\sigma(\Xiccp)\times\BF(\Xiccp \to
  \Lc\Km\pip)}{\sigma(\Xiccpp)\times\BF(\Xiccpp \to \Lc\Km\pip\pip)}
\end{equation}
relative to the $\Xiccpp \to \Lc\Km\pip\pip$ decay,
where $\sigma$ is the production cross-section and
$\mathcal{B}$ is the decay branching fraction.
The determination of the ratio $\mathcal{R}(\Lc)$ allows a direct comparison with previous experiments,
while that of $\mathcal{R}(\Xiccpp)$ provides information 
about the ratio of the branching fractions of the 
$\Xiccp \to \Lc\Km\pip$ and $\Xiccpp \to \Lc\Km\pip\pip$ decays
assuming that the members of the isospin doublet have a similar production cross-section~\cite{Kiselev:2001fw, Ma:2003zk, Chang:2006eu}. 
The production ratios are evaluated as
\begin{equation}
  \mathcal{R} = \frac{\varepsilon_{\text{norm}}}{\varepsilon_{\text{sig}}}
                \frac{N_{\text{sig}}}{N_{\text{norm}}}
              \equiv \alpha N_{\text{sig}},
\end{equation}
where $\varepsilon_{\text{sig}}$ and $\varepsilon_{\text{norm}}$ 
refer to the selection efficiencies of the \Xiccp signal decay mode
and the \Lc or \Xiccpp normalisation decay modes respectively,
$N_{\text{sig}}$ and $N_{\text{norm}}$ are the corresponding yields, and $\alpha$
is the single-event sensitivity. 
Because the \Xiccp selection efficiency depends strongly on the lifetime,
limits on $\mathcal{R}(\Lc)$ and $\mathcal{R}(\Xiccpp)$
are quoted as functions of the \Xiccp signal mass 
for a discrete set of lifetime hypotheses.

